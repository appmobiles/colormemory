package com.dut2.colormemory.models;

import java.util.Calendar;
import java.util.Date;

public class Utilisateur {

    private String pseudo, genre;
    private Date dateNaissance;

    public Utilisateur(String pseudo, String genre, Date dateNaissance) {

        this.pseudo = pseudo;
        this.genre = genre;
        this.dateNaissance = dateNaissance;
    }

    public Utilisateur() {}



    //GETTERS
    public String getPseudo() {
        return pseudo;
    }
    public String getGenre() {
        return genre;
    }
    public Date getDateNaissance() {
        return dateNaissance;
    }
    public int getAge() {
        Calendar curr = Calendar.getInstance();
        Calendar birth = Calendar.getInstance();
        birth.setTime(this.dateNaissance);
        int age = curr.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
        curr.add(Calendar.YEAR,-age);
        if(birth.after(curr)) {
            age = age - 1;
        }
        return age;
    }

    //SETTER
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
}
