package com.dut2.colormemory.models;

public class JoueurItem {

    private int rang;
    private double totalPoint;
    private String pseudo, id;


    public JoueurItem(int rang, String pseudo, double totalPoint, String id) {
        this.rang = rang;
        this.pseudo = pseudo;
        this.totalPoint = totalPoint;
        this.id = id;
    }


    public int getRang() {
        return rang;
    }

    public String getPseudo() {
        return pseudo;
    }

    public double getTotalPoint() {
        return totalPoint;
    }

    public String getId() {
        return id;
    }
}
