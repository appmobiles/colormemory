package com.dut2.colormemory.models;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.dut2.colormemory.Niveaux;

public class BoutonJeu {

    private int couleurAllumee, couleurEteinte;
    private int boutonPositionTableau;

    private Button bouton;
    private MediaPlayer sonbouton;
    private Niveaux niveaux;


    public BoutonJeu(Button bouton, int nbBouton, int boutonPositionTableau, MediaPlayer sonbouton, Niveaux niveaux) {

        this.bouton = bouton;
        this.boutonPositionTableau = boutonPositionTableau;
        this.sonbouton = sonbouton;

        this.couleurAllumee = Color.parseColor(String.valueOf(this.bouton.getTag()));
        this.couleurEteinte = calculCouleurEteinte(couleurAllumee);
        this.niveaux = niveaux;

        definirInterraction();

        if (boutonPositionTableau>nbBouton-1) {
            this.bouton.setEnabled(false);
            this.bouton.setBackgroundColor(0);
        }
        else {
            eteindre();
        }
    }


    private int calculCouleurEteinte(int couleurAllumee) {
        float[] hsv = new float[3];
        Color.colorToHSV(couleurAllumee, hsv);
        hsv[2] *= 0.5f;
        return Color.HSVToColor(hsv);
    }


    public void allumer() {
        this.bouton.setBackgroundColor(this.couleurAllumee);
        sonbouton.start();
    }

    public void eteindre() {
        this.bouton.setBackgroundColor(this.couleurEteinte);
    }

    public void afficher() {
        this.bouton.setEnabled(true);
        eteindre();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void definirInterraction() {
        this.bouton.setOnTouchListener(new View.OnTouchListener() {
            private boolean tourjoueur;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tourjoueur = niveaux.getTourJoueur();
                if (tourjoueur == true) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            allumer();
                            break;

                        case MotionEvent.ACTION_UP:
                            eteindre();
                            niveaux.verificationAction(boutonPositionTableau);
                            break;
                    }
                }
                return true;
            }
        });
    }
}
