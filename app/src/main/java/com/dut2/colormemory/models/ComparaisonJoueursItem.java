package com.dut2.colormemory.models;

public class ComparaisonJoueursItem {


    private int nbPartieUtilisateur;
    private double nbPointUtilisateur, moyenneUtilisateur, scoreMaxUtilisateur;
    private int nbPartieAdversaire;
    private double nbPointAdversaire, moyenneAdversaire, scoreMaxAdversaire;

    private String categorie;


    public ComparaisonJoueursItem(String categorie, double nbPointUtilisateur, int nbPartieUtilisateur, double moyenneUtilisateur, double scoreMaxUtilisateur,
                                                    double nbPointAdversaire, int nbPartieAdversaire, double moyenneAdversaire, double scoreMaxAdversaire) {

        this.categorie = categorie;

        this.nbPointUtilisateur = nbPointUtilisateur;
        this.nbPartieUtilisateur = nbPartieUtilisateur;
        this.moyenneUtilisateur = moyenneUtilisateur;
        this.scoreMaxUtilisateur = scoreMaxUtilisateur;

        this.nbPointAdversaire = nbPointAdversaire;
        this.nbPartieAdversaire = nbPartieAdversaire;
        this.moyenneAdversaire = moyenneAdversaire;
        this.scoreMaxAdversaire = scoreMaxAdversaire;
    }


    public String getCategorie() {
        return categorie;
    }



    public double getNbPointUtilisateur() {
        return nbPointUtilisateur;
    }

    public int getNbPartieUtilisateur() {
        return nbPartieUtilisateur;
    }

    public double getScoreMaxUtilisateur() {
        return scoreMaxUtilisateur;
    }

    public double getMoyenneUtilisateur() {
        return moyenneUtilisateur;
    }


    public double getNbPointAdversaire() {
        return nbPointAdversaire;
    }

    public int getNbPartieAdversaire() {
        return nbPartieAdversaire;
    }

    public double getScoreMaxAdversaire() {
        return scoreMaxAdversaire;
    }

    public double getMoyenneAdversaire() {
        return moyenneAdversaire;
    }
}
