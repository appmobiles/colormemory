package com.dut2.colormemory.models;

public class UtilisateurDonneesJeux {

    private int facileNbPartie;
    private double facilePoints, facileScoreMax;
    private int difficileNbPartie;
    private double difficilePoints, difficileScoreMax;
    private int expertNbPartie;
    private double expertPoints, expertScoreMax;
    private int chronoNbPartie;
    private double chronoPoints, chronoScoreMax;

    public UtilisateurDonneesJeux(int facileNbPartie, int difficileNbPartie, int expertNbPartie, int chronoNbPartie,
                                  double facilePoints, double difficilePoints, double expertPoints, double chronoPoints,
                                  double facileScoreMax, double difficileScoreMax, double expertScoreMax, double chronoScoreMax) {

        this.facileNbPartie = facileNbPartie;
        this.difficileNbPartie = difficileNbPartie;
        this.expertNbPartie = expertNbPartie;
        this.chronoNbPartie = chronoNbPartie;

        this.facilePoints = facilePoints;
        this.difficilePoints = difficilePoints;
        this.expertPoints = expertPoints;
        this.chronoPoints = chronoPoints;

        this.facileScoreMax = facileScoreMax;
        this.difficileScoreMax = difficileScoreMax;
        this.expertScoreMax = expertScoreMax;
        this.chronoScoreMax = chronoScoreMax;
    }

    public UtilisateurDonneesJeux() {}



    //GETTERS
    public int getFacileNbPartie() {
        return facileNbPartie;
    }
    public int getDifficileNbPartie() {
        return difficileNbPartie;
    }
    public int getExpertNbPartie() {
        return expertNbPartie;
    }
    public int getChronoNbPartie() {
        return chronoNbPartie;
    }
    public int getTotalNbPartie() {
        return facileNbPartie + difficileNbPartie + expertNbPartie + chronoNbPartie;
    }

    public double getFacilePoints() {
        return facilePoints;
    }
    public double getDifficilePoints() {
        return difficilePoints;
    }
    public double getExpertPoints() {
        return expertPoints;
    }
    public double getChronoPoints() {
        return chronoPoints;
    }
    public double getTotalPoint() {
        return facilePoints + difficilePoints + expertPoints + chronoPoints;
    }

    public double getFacileScoreMax() {
        return facileScoreMax;
    }
    public double getDifficileScoreMax() {
        return difficileScoreMax;
    }
    public double getExpertScoreMax() {
        return expertScoreMax;
    }
    public double getChronoScoreMax() {
        return chronoScoreMax;
    }
    public double getTotalScoreMax() {
        double scoreMax;
        if( facileScoreMax > difficileScoreMax && facileScoreMax > expertScoreMax && facileScoreMax >chronoScoreMax) {
            scoreMax = facileScoreMax;
        }
        else if (difficileScoreMax > expertScoreMax && difficileScoreMax > chronoScoreMax) {
            scoreMax = difficileScoreMax;
        }
        else if (expertScoreMax > chronoScoreMax) {
            scoreMax = expertScoreMax;
        }
        else {
            scoreMax = chronoScoreMax;
        }
        return scoreMax;
    }

    public double getFacileMoyenne() {
        double moyenne;
        if (facileNbPartie == 0) {
            moyenne = 0;
        }
        else {
            moyenne = facilePoints/facileNbPartie;
            moyenne = Math.round(moyenne*100);
            moyenne = moyenne/100;
        }
        return moyenne;
    }
    public double getDifficileMoyenne() {
        double moyenne;
        if (difficileNbPartie == 0) {
            moyenne = 0;
        }
        else {
            moyenne = difficilePoints/difficileNbPartie;
            moyenne = Math.round(moyenne*100);
            moyenne = moyenne/100;
        }
        return moyenne;
    }
    public double getExpertMoyenne() {
        double moyenne;
        if (expertNbPartie == 0) {
            moyenne = 0;
        }
        else {
            moyenne = expertPoints/expertNbPartie;
            moyenne = Math.round(moyenne*100);
            moyenne = moyenne/100;
        }
        return moyenne;
    }
    public double getChronoMoyenne() {
        double moyenne;
        if (chronoNbPartie == 0) {
            moyenne = 0;
        }
        else {
            moyenne = chronoPoints/chronoNbPartie;
            moyenne = Math.round(moyenne*100);
            moyenne = moyenne/100;
        }
        return moyenne;
    }
    public double getTotalMoyenne() {
        double moyenne;
        int totalNbParties = getTotalNbPartie();
        if (totalNbParties == 0) {
            moyenne = 0;
        }
        else {
            moyenne = getTotalPoint()/totalNbParties;
            moyenne = Math.round(moyenne*100);
            moyenne = moyenne/100;
        }
        return moyenne;
    }





    //SETTER
    public void setFacileNbPartie(int facileNbPartie) {
        this.facileNbPartie = facileNbPartie;
    }
    public void setDifficileNbPartie(int difficileNbPartie) {
        this.difficileNbPartie = difficileNbPartie;
    }
    public void setExpertNbPartie(int expertNbPartie) {
        this.expertNbPartie = expertNbPartie;
    }
    public void setChronoNbPartie(int chronoNbPartie) {
        this.chronoNbPartie = chronoNbPartie;
    }

    public void setFacilePoints(double facilePoints) {
        this.facilePoints = facilePoints;
    }
    public void setDifficilePoints(double difficilePoints) {
        this.difficilePoints = difficilePoints;
    }
    public void setExpertPoints(double expertPoints) {
        this.expertPoints = expertPoints;
    }
    public void setChronoPoints(double chronoPoints) {
        this.chronoPoints = chronoPoints;
    }

    public void setFacileScoreMax(double facileScoreMax) {
        this.facileScoreMax = facileScoreMax;
    }
    public void setDifficileScoreMax(double difficileScoreMax) {
        this.difficileScoreMax = difficileScoreMax;
    }
    public void setExpertScoreMax(double expertScoreMax) {
        this.expertScoreMax = expertScoreMax;
    }
    public void setChronoScoreMax(double chronoScoreMax) {
        this.chronoScoreMax = chronoScoreMax;
    }
}
