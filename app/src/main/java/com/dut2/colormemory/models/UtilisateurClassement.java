package com.dut2.colormemory.models;

public class UtilisateurClassement {

    private double totalPoint;
    private String pseudo;



    public UtilisateurClassement(double totalPoint, String pseudo) {
        this.totalPoint = totalPoint;
        this.pseudo = pseudo;
    }

    public UtilisateurClassement() {}



    //GETTERS
    public double getTotalPoint() {
        return totalPoint;
    }
    public String getPseudo() {
        return pseudo;
    }



    //SETTER
    public void setTotalPoint(double totalPoint) {
        this.totalPoint = totalPoint;
    }
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
}
