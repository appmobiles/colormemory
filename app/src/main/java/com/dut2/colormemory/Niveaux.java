package com.dut2.colormemory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;

import com.dut2.colormemory.api.UtilisateurClassementHelper;
import com.dut2.colormemory.api.UtilisateurDonneesJeuxHelper;
import com.dut2.colormemory.base.Base;
import com.dut2.colormemory.models.BoutonJeu;
import com.dut2.colormemory.models.UtilisateurDonneesJeux;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

/*
 * Il s'agit du jeu en lui meme
 */
public class Niveaux extends Base {

    private boolean premierAffichage = true;
    private boolean tourJoueur = false;
    private boolean rejouer = false;

    private int niveau = 0;
    private int nbBonneAction = 0;
    private int nbVie;

    private double score = 0;

    private int [] idBoutonsColores;
    private double [] donneesMode;

    private TextView tempsRestantTextView, scoreTextView;
    private ImageView coeur1ImageView, coeur2ImageView, coeur3ImageView;
    private Group boutonsColoresGroup;
    private MediaPlayer sonboutonMediaPlayer;

    private BoutonJeu[] boutonsJeu;
    private ArrayList<Integer> suiteCouleur = new ArrayList<Integer>();

    private Runnable sequenceLumieres;
    private Niveaux niveaux;
    private String typeFin;

    private UtilisateurDonneesJeux utilisateurDonneesJeux;





    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_niveaux);

        updateDonneeClassement(false);
        donneesMode = getIntent().getDoubleArrayExtra("donnéesMode");

        this.nbVie = (int)donneesMode[2];
        this.niveaux = Niveaux.this;
        this.typeFin = getString(R.string.perdu);


        tempsRestantTextView = findViewById(R.id.id_niveaux_textView_tempsRestant);
        if (!(donneesMode[1] == 0)) {
            tempsRestantTextView.setVisibility(View.GONE);
        }
        scoreTextView = findViewById(R.id.id_niveaux_textView_score);


        coeur1ImageView = findViewById(R.id.id_niveaux_imageView_coeur1);
        coeur2ImageView = findViewById(R.id.id_niveaux_imageView_coeur2);
        coeur3ImageView = findViewById(R.id.id_niveaux_imageView_coeur3);
        if (nbVie == 2) {
            coeur3ImageView.setImageResource(R.drawable.heart_vide);
        }

        sonboutonMediaPlayer = MediaPlayer.create(this, R.raw.sonbouton);
        boutonsColoresGroup = findViewById(R.id.id_niveaux_group_boutonsColores);
        idBoutonsColores = boutonsColoresGroup.getReferencedIds();
        boutonsJeu = new BoutonJeu[10];
        for (int i=0;i<10;i++) {
            boutonsJeu[i] = new BoutonJeu(findViewById(idBoutonsColores[i]), 4, i, sonboutonMediaPlayer, niveaux);
        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_niveaux;
    }







    private void commencer() {
        genererCouleur();
        premierAffichage = false;
    }

    public boolean getTourJoueur() {
        return this.tourJoueur;
    }

    public void setTourJoueur(boolean tourJoueur) {
        this.tourJoueur = tourJoueur;
    }

    public void fin() {
        this.tourJoueur = false;
        updateDonneesNiveau();
    }

    private void genererCouleur() {
        int nbCreationCouleur;

        if (rejouer == true) {
            suiteCouleur.clear();
            rejouer = false;
        }

        //Definie la suite de couleur
        nbCreationCouleur = 1;
        if (suiteCouleur.size()<donneesMode[0]) {
            nbCreationCouleur = (int)donneesMode[0]-suiteCouleur.size() ;
        }

        for (int y=0; y<nbCreationCouleur; y++) {
            suiteCouleur.add((int) (Math.random() * (4+niveau)));
        }

        sequenceLumieres = new SequenceLumieres(premierAffichage, suiteCouleur, boutonsJeu, niveaux, tempsRestantTextView, donneesMode);
        new Thread(sequenceLumieres).start();

        rejouer = false;
    }

    public void verificationAction(int boutonPositionTableau) {
        if(boutonPositionTableau == suiteCouleur.get(nbBonneAction)) { //Si l'utilisateur a fait une bonne action
            nbBonneAction++;
            if (nbBonneAction==suiteCouleur.size()) {
                nbBonneAction=0;
                tourJoueur = false;

                if (donneesMode[1]==0) {
                    score = Math.round((suiteCouleur.size()*donneesMode[3]+score) * 100);
                    score = score /100;
                    scoreTextView.setText(String.valueOf(score));
                }

                if (suiteCouleur.size() == donneesMode[1]) { //Le ArrayList ne passe pas ici quand ca taille est de 0, le mode chrono ne valide donc jamais cette condition
                    niveau++;
                    score = Math.round((niveau*donneesMode[3]+score) * 100);
                    score = score /100;
                    scoreTextView.setText(String.valueOf(score));

                    if (niveau > 6) { // 6 car il y a 6 niveaux (commence a 0 mais implementé au dessus
                        typeFin = getString(R.string.gagne);
                        fin();
                    }
                    else {
                        suiteCouleur.clear();
                        boutonsJeu[3+niveau].afficher();

                        genererCouleur();
                    }
                }
                else {
                    genererCouleur();
                }
            }
        }
        else { //Si l'utilisateur a fait une mauvaise action
            tourJoueur = false;
            nbVie--;
            suiteCouleur.clear();

            if (nbVie == 2) {
                coeur3ImageView.setImageResource(R.drawable.heart_vide);
            }
            else if (nbVie == 1) {
                coeur2ImageView.setImageResource(R.drawable.heart_vide);
            }
            else {
                coeur1ImageView.setImageResource(R.drawable.heart_vide);
            }

            if (nbVie > 0) {
                nbBonneAction = 0;
                rejouer = true;
                genererCouleur();
            }
            else{
                fin();
            }
        }
    }



    private void updateDonneesNiveau() { //Met les point gagner et enregistre les donnees dans le mode qui correspond
        if (donneesMode[3] == 1) {      //J'utlise le poid pour differencier les modes car si 2 mode ont la meme valeur, il n'y a aucune utlité a jouer au plus dur.
            double scoreMax = utilisateurDonneesJeux.getFacileScoreMax();
            if (scoreMax < score) {
                scoreMax = score;
            }
            UtilisateurDonneesJeuxHelper.updateUtilisateurDonneesJeuxFacile(getUtilisateurConnecte().getUid(), utilisateurDonneesJeux.getFacileNbPartie()+1,
                    utilisateurDonneesJeux.getFacilePoints()+score, scoreMax).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    updateDonneeClassement(true);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    onFailureListener();
                    changementActivite();
                }
            });
        }
        else if (donneesMode[3] == 1.5) {
            double scoreMax = utilisateurDonneesJeux.getDifficileScoreMax();
            if (scoreMax < score) {
                scoreMax = score;
            }
            UtilisateurDonneesJeuxHelper.updateUtilisateurDonneesJeuxDifficile(getUtilisateurConnecte().getUid(), utilisateurDonneesJeux.getDifficileNbPartie()+1,
                    utilisateurDonneesJeux.getDifficilePoints()+score, scoreMax).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    updateDonneeClassement(true);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    onFailureListener();
                    changementActivite();
                }
            });
        }
        else  if (donneesMode[3] == 3) {
            double scoreMax = utilisateurDonneesJeux.getExpertScoreMax();
            if (scoreMax < score) {
                scoreMax = score;
            }
            UtilisateurDonneesJeuxHelper.updateUtilisateurDonneesJeuxExpert(getUtilisateurConnecte().getUid(), utilisateurDonneesJeux.getExpertNbPartie()+1,
                    utilisateurDonneesJeux.getExpertPoints()+score, scoreMax).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    updateDonneeClassement(true);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    onFailureListener();
                    changementActivite();
                }
            });
        }
        else {
            double scoreMax = utilisateurDonneesJeux.getChronoScoreMax();
            if (scoreMax < score) {
                scoreMax = score;
            }
            UtilisateurDonneesJeuxHelper.updateUtilisateurDonneesJeuxChrono(getUtilisateurConnecte().getUid(), utilisateurDonneesJeux.getChronoNbPartie()+1,
                    utilisateurDonneesJeux.getChronoPoints()+score, scoreMax).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    updateDonneeClassement(true);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    onFailureListener();
                    changementActivite();
                }
            });
        }
    }


    private void updateDonneeClassement(boolean changementActivite) { //Met a jour le total des points gagner pour le classement
        UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeux(getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                utilisateurDonneesJeux = documentSnapshot.toObject(UtilisateurDonneesJeux.class);
                UtilisateurClassementHelper.updateTotalPoint(getUtilisateurConnecte().getUid(), utilisateurDonneesJeux.getTotalPoint());
                if (changementActivite == true) {
                    changementActivite();
                }
                else {
                    commencer();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
                changementActivite();
            }
        });
    }



    private void changementActivite() {
        Intent intent = new Intent(Niveaux.this, PostNiveaux.class);
        intent.putExtra("donneesModeUtilise", donneesMode);
        intent.putExtra("score", score);
        intent.putExtra("typeFin", this.typeFin);
        startActivity(intent);
    }









    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {

    }
}