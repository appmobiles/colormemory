package com.dut2.colormemory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.dut2.colormemory.api.UtilisateurClassementHelper;
import com.dut2.colormemory.api.UtilisateurDonneesJeuxHelper;
import com.dut2.colormemory.api.UtilisateurHelper;
import com.dut2.colormemory.base.Base;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Permet a l'utilisateur de s'inscrire
 */

public class Inscription extends Base {

    private String pseudo, email, mdp, mdpConfirmation, dateNaissance, genre;

    private TextView seConnecterTextView;
    private EditText pseudoEditText, emailEditText, mdpEditText, mdpConfirmationEditText, dateNaissanceEditText;
    private CheckBox filleCheckBox, garconCheckBox, autreCheckBox;
    private Button sInscrireButton;
    @SuppressLint("SimpleDateFormat")
    static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);


        pseudoEditText = findViewById(R.id.id_inscription_editText_pseudo);
        emailEditText = findViewById(R.id.id_inscription_editText_email);
        mdpEditText = findViewById(R.id.id_inscription_editText_mdp);
        mdpConfirmationEditText = findViewById(R.id.id_inscription_editText_mdpConfirmation);
        dateNaissanceEditText = findViewById(R.id.id_inscription_editText_dateNaissance);
        filleCheckBox = findViewById(R.id.id_inscription_checkBox_fille);
        garconCheckBox = findViewById(R.id.id_inscription_checkBox_garcon);
        autreCheckBox = findViewById(R.id.id_inscription_checkBox_autre);
        seConnecterTextView = findViewById(R.id.id_inscription_textView_seConnecter);
        sInscrireButton = findViewById(R.id.id_inscription_button_sInscrire);


        /* DEBUT
         * Permet a l'utilisateur de choisir son genre
         */
        filleCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filleCheckBox.setChecked(true);
                garconCheckBox.setChecked(false);
                autreCheckBox.setChecked(false);
                genre = "fille";
            }
        });
        garconCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filleCheckBox.setChecked(false);
                garconCheckBox.setChecked(true);
                autreCheckBox.setChecked(false);
                genre = "garcon";
            }
        });
        autreCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filleCheckBox.setChecked(false);
                garconCheckBox.setChecked(false);
                autreCheckBox.setChecked(true);
                genre = "autre";
            }
        });

        /* FIN
         * Permet a l'utilisateur de choisir son genre
         */



        sInscrireButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pseudo = pseudoEditText.getText().toString();
                email = emailEditText.getText().toString();
                mdp = mdpEditText.getText().toString();
                mdpConfirmation = mdpConfirmationEditText.getText().toString();
                dateNaissance = dateNaissanceEditText.getText().toString();

                //Verification des differents champs saisie
                if (pseudo.isEmpty()) {
                    Toast.makeText(Inscription.this, getString(R.string.veuillez_renseigner_un_pseudo), Toast.LENGTH_SHORT).show();
                }
                else if (email.isEmpty()) {
                    Toast.makeText(Inscription.this, getString(R.string.veuillez_renseigner_un_email), Toast.LENGTH_SHORT).show();
                }
                else if (mdp.isEmpty() || mdpEditText.length()<6) {
                    Toast.makeText(Inscription.this, getString(R.string.veuillez_renseigner_un_mot_de_passe), Toast.LENGTH_SHORT).show();
                }
                else if (!mdpConfirmation.equals(mdp)) {
                    Toast.makeText(Inscription.this, getString(R.string.confirmation_du_mot_de_passe_incorrecte), Toast.LENGTH_SHORT).show();
                }
                else if (dateNaissance.isEmpty()) {
                    Toast.makeText(Inscription.this, getString(R.string.veuillez_renseigner_votre_date_de_naissance), Toast.LENGTH_SHORT).show();
                }
                else if (genre == null) {
                    Toast.makeText(Inscription.this, getString(R.string.veuillez_renseigner_votre_genre), Toast.LENGTH_SHORT).show();
                }
                else {
                    ajoutUtilisateur();
                }
            }
        });



        seConnecterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Inscription.this, Connexion.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_inscription;
    }




    //Permet de mettre la date avec le bon format
    private Date getDateNaissance(){
        try {
            Date date = dateFormat.parse(dateNaissance);
            return date ;
        } catch (ParseException e){
            return new Date() ;
        }
    }





    private void ajoutUtilisateur() {
        getmAuth().createUserWithEmailAndPassword(email, mdp)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //ajoutDonneeConfidentielle();
                            creerUtilisateur();
                        } else {
                            onFailureListener();
                        }
                    }
                });
    }

    //Initialise les donnees du joueur
    private void creerUtilisateur() {
        UtilisateurHelper.creerUtilisateur( getmAuth().getUid(), pseudo, genre, getDateNaissance()).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
        UtilisateurDonneesJeuxHelper.creerUtilisateurDonneesJeux(getmAuth().getUid()).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
        UtilisateurClassementHelper.creerUtilisateurClassement(getmAuth().getUid(), pseudo).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
        inscriptionTermine();
    }

    private void inscriptionTermine() {
        Toast.makeText(Inscription.this, getString(R.string.votre_compte_a_ete_cree_avec_succes), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Inscription.this, Connexion.class);
        startActivity(intent);
    }









    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {
        Intent intent = new Intent( Inscription.this, Connexion.class);
        startActivity(intent);
    }
}