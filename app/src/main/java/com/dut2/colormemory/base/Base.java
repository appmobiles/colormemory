package com.dut2.colormemory.base;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dut2.colormemory.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public abstract class Base extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(this.getFragmentLayout());
    }

    protected abstract int getFragmentLayout();










    protected void onFailureListener(){
        Toast.makeText(getApplicationContext(), getString(R.string.une_erreur_est_survenue), Toast.LENGTH_LONG).show();
    }





    @Nullable
    protected FirebaseUser getUtilisateurConnecte(){ return FirebaseAuth.getInstance().getCurrentUser(); }

    protected FirebaseAuth getmAuth() {
        return FirebaseAuth.getInstance();
    }

    protected Boolean isConnecte(){ return (this.getUtilisateurConnecte() != null); }
}
