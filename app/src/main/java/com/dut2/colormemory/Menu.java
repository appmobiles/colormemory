package com.dut2.colormemory;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.dut2.colormemory.api.UtilisateurClassementHelper;
import com.dut2.colormemory.api.UtilisateurDonneesJeuxHelper;
import com.dut2.colormemory.api.UtilisateurHelper;
import com.dut2.colormemory.base.Base;
import com.dut2.colormemory.models.JoueurItem;
import com.dut2.colormemory.models.Utilisateur;
import com.dut2.colormemory.models.UtilisateurDonneesJeux;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

/*
 * Menu de l'application dont le classement et le choix du niveau
 */

public class Menu extends Base {

    private int niveauChoisi;
    private double [] donneesMode;

    private TextView totalPointTextView, pseudoTextView;
    private Button boutonJouerButton;
    private TabLayout choixNiveauTabLayout;
    private ListView classementListView;

    private List<JoueurItem> joueurItemList;
    private Menu context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        pseudoTextView = findViewById(R.id.id_menu_textView_pseudo);
        totalPointTextView = findViewById(R.id.id_menu_textView_totalPoints);
        afficherDonneeUtilisateur();

        boutonJouerButton = findViewById(R.id.id_menu_button_jouer);
        choixNiveauTabLayout = findViewById(R.id.id_menu_tabLayout_choixNiveau);
        classementListView = findViewById(R.id.id_menu_listView_classement);

        context = Menu.this;
        joueurItemList = new ArrayList<>();


        classement();


        boutonJouerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                /* DEBUT
                 * Recupere les info du niveau selectionne
                 */
                niveauChoisi = choixNiveauTabLayout.getSelectedTabPosition();
                switch (niveauChoisi) {
                    case 1: //Difficile
                        /*nbMinIllumination = 3; nbMaxIllumination = 15; nbVie = 2; poidMode = 1.5;*/
                        donneesMode = new double[]{3, 15, 2, 1.5};

                        break;
                    case 2: //Expert
                        /*nbMinIllumination = 5; nbMaxIllumination = 20; nbVie = 3; poidMode = 3;*/
                        donneesMode = new double[]{5, 20, 3, 3};

                        break;
                    case 3: //Chrono
                        /*nbMinIllumination = 5; nbMaxIllumination = 20; nbVie = 3; poidMode = 3;*/
                        donneesMode = new double[]{1, 0, 3, 0.15};

                        break;
                    default: //Facile (et autre si pbr)
                        /*nbMinIllumination = 1; nbMaxIllumination = 10; nbVie = 2; poidMode = 1;*/
                        donneesMode = new double[]{1, 10, 2, 1};

                        break;
                    }
                    /* FIN
                     * Recupere les info du niveau selectionne
                     */

                    Intent intent = new Intent(Menu.this, Niveaux.class);
                    intent.putExtra("donnéesMode", donneesMode);
                    startActivity(intent);
            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_menu;
    }


    private void afficherDonneeUtilisateur() {
        UtilisateurHelper.getUtilisateur(this.getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Utilisateur utilisateur = documentSnapshot.toObject(Utilisateur.class);
                pseudoTextView.setText(utilisateur.getPseudo());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
        UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeux(getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                UtilisateurDonneesJeux utilisateurDonneesJeux = documentSnapshot.toObject(UtilisateurDonneesJeux.class);
                totalPointTextView.setText(String.valueOf(utilisateurDonneesJeux.getTotalPoint()));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
    }



    //Recupere au maximum les 25 premiers joueur et les affiche dans le classement
    private void classement() {
        UtilisateurClassementHelper.getUtilisateursClassement()
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    int i = 1;
                    double scorePrecedent = 0;
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        double scoreActuelle = document.getDouble("totalPoint");
                        if (scorePrecedent>scoreActuelle) {
                            i++;
                        }
                        joueurItemList.add(new JoueurItem(i, document.getString("pseudo"), scoreActuelle, document.getId()));
                        scorePrecedent = scoreActuelle;
                    }
                    classementListView.setAdapter(new JoueurItemAdapter(context, joueurItemList));
                } else {
                    onFailureListener();
                }
            }
        });
    }










    /* DEBUT
     * Gestion de la barre de navigation
     */

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.id_menu_navigation_profil:
                intent = new Intent(this, Profil.class);
                startActivity(intent);
                return true;
            case R.id.id_menu_navigation_deconnexion:
                getmAuth().signOut();
                intent = new Intent(this, Connexion.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* FIN
     * Gestion de la barre de navigation
     */


    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {

    }
}