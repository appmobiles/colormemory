package com.dut2.colormemory;

import android.os.Handler;
import android.widget.TextView;

import com.dut2.colormemory.models.BoutonJeu;

import java.util.ArrayList;

public class SequenceLumieres implements Runnable {

    private boolean premierAffichage, tourJoueur;
    private int tempsRestant;
    private double [] donneesMode;

    private ArrayList<Integer> suiteCouleur;
    private TextView tempsRestantTextView;
    private BoutonJeu[] boutonsJeu;
    private Niveaux niveaux;
    private Handler handler = new Handler();



    public SequenceLumieres(boolean premierAffichage, ArrayList<Integer> suiteCouleur, BoutonJeu[] boutonsJeu, Niveaux niveaux, TextView tempsRestantTextView, double[] donneesMode) {
        this.premierAffichage = premierAffichage;
        this.suiteCouleur = suiteCouleur;
        this.boutonsJeu = boutonsJeu;
        this.niveaux = niveaux;

        this.tempsRestant = suiteCouleur.size()*2000;
        this.tempsRestantTextView = tempsRestantTextView;
        this.donneesMode = donneesMode;
    }



    public void run() {
        if (premierAffichage == true) {
            try { Thread.sleep(1000); }
            catch (Exception e) {}
        }
        //Delai avant de commencer la suite d'illumination
        try { Thread.sleep(750); }
        catch (Exception e) {}


        //Affiche le temps qu'aura le joueur s'il est en mode difficile
        if (donneesMode[1]==0) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    tempsRestantTextView.setText(String.valueOf((double) tempsRestant/1000));
                }
            });
        }



        // Pour chaque illumination
        for (int i=0; i<suiteCouleur.size(); i++) {
            //Allumer et attendre
            boutonsJeu[suiteCouleur.get(i)/*[i]*/].allumer();
            try { Thread.sleep(1000); }
            catch (Exception e) {}

            //Eteindre et attendre
            boutonsJeu[suiteCouleur.get(i)/*[i]*/].eteindre();
            if (i<suiteCouleur.size()-1) {
                try { Thread.sleep(250); }
                catch (Exception e) {}
            }
        }
        tourJoueur = true;
        niveaux.setTourJoueur(tourJoueur);


        //Fait diminuer le temps qu'a le joueur s'il est en mode difficile
        if (donneesMode[1]==0) { //Si le mode courant est le chrono
            while (tempsRestant>0 && niveaux.getTourJoueur()==true) {
                tempsRestant = tempsRestant - 100;
                if (tempsRestant<1) {
                    niveaux.fin();
                }
                else {
                    try { Thread.sleep(100); }
                    catch (InterruptedException e) {}

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tempsRestantTextView.setText(String.valueOf((double) tempsRestant/1000));
                        }
                    });
                }
            }
        }
    }
}
