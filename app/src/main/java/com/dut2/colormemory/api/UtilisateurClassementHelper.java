package com.dut2.colormemory.api;

import com.dut2.colormemory.models.UtilisateurClassement;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class UtilisateurClassementHelper {

    private static final String COLLECTION_NAME = "Classement";

    // --- COLLECTION REFERENCE ---

    public static CollectionReference getUtilisateurClassementCollection(){
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---

    public static Task<Void> creerUtilisateurClassement(String uid, String pseudo) {
        UtilisateurClassement utilisateurClassement = new UtilisateurClassement(0, pseudo);
        return UtilisateurClassementHelper.getUtilisateurClassementCollection().document(uid).set(utilisateurClassement);
    }

    // --- GET ---

    public static Query getUtilisateursClassement(){
        return UtilisateurClassementHelper.getUtilisateurClassementCollection().orderBy("totalPoint", Query.Direction.DESCENDING).limit(25);
    }

    // --- UPDATE ---

    public static Task<Void> updatePseudo(String uid, String pseudo) {
        return UtilisateurClassementHelper.getUtilisateurClassementCollection().document(uid).update("pseudo", pseudo);
    }

    public static Task<Void> updateTotalPoint(String uid, double totalScore) {
        return UtilisateurClassementHelper.getUtilisateurClassementCollection().document(uid).update("totalPoint", totalScore);
    }

    // --- DELETE ---

    public static Task<Void> supprimerUtilisateurClassement(String uid) {
        return UtilisateurClassementHelper.getUtilisateurClassementCollection().document(uid).delete();
    }
}
