package com.dut2.colormemory.api;

import com.dut2.colormemory.models.UtilisateurDonneesJeux;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class UtilisateurDonneesJeuxHelper {

    private static final String COLLECTION_NAME = "DonneesJeux";

    // --- COLLECTION REFERENCE ---

    public static CollectionReference getUtilisateurDonneesJeuxCollection(){
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---

    public static Task<Void> creerUtilisateurDonneesJeux(String uid) {
        UtilisateurDonneesJeux utilisateurDonneesJeux = new UtilisateurDonneesJeux( 0, 0, 0, 0,
                                                                                    0, 0, 0, 0,
                                                                                    0, 0, 0, 0);
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).set(utilisateurDonneesJeux);
    }

    // --- GET ---

    public static Task<DocumentSnapshot> getUtilisateurDonneesJeux(String uid){
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).get();
    }

    // --- UPDATE ---

    public static Task<Void> updateUtilisateurDonneesJeuxFacile(String uid, int facileNbPartie, double facilePoints, double facileScoreMax) {
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).update( "facileNbPartie", facileNbPartie, "facilePoints", facilePoints, "facileScoreMax", facileScoreMax);
    }

    public static Task<Void> updateUtilisateurDonneesJeuxDifficile(String uid, int difficileNbPartie, double difficilePoints, double difficileScoreMax) {
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).update("difficileNbPartie", difficileNbPartie, "difficilePoints", difficilePoints, "difficileScoreMax", difficileScoreMax);
    }

    public static Task<Void> updateUtilisateurDonneesJeuxExpert(String uid, int expertNbPartie, double expertPoints, double expertScoreMax) {
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).update( "expertNbPartie", expertNbPartie, "expertPoints", expertPoints, "expertScoreMax", expertScoreMax);
    }

    public static Task<Void> updateUtilisateurDonneesJeuxChrono(String uid, int chronoNbPartie, double chronoPoints, double chronoScoreMax) {
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).update( "chronoNbPartie", chronoNbPartie, "chronoPoints", chronoPoints, "chronoScoreMax", chronoScoreMax);
    }

    // --- DELETE ---

    public static Task<Void> supprimerUtilisateurDonneesJeux(String uid) {
        return UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeuxCollection().document(uid).delete();
    }
}
