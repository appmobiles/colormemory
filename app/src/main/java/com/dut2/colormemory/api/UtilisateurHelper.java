package com.dut2.colormemory.api;

import com.dut2.colormemory.models.Utilisateur;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;

public class UtilisateurHelper {

    private static final String COLLECTION_NAME = "Confidentielle";

    // --- COLLECTION REFERENCE ---

    public static CollectionReference getUtilisateurCollection(){
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---

    public static Task<Void> creerUtilisateur(String uid, String pseudo, String genre, Date dateNaissance) {
        Utilisateur utilisateur = new Utilisateur(pseudo, genre, dateNaissance);
        return UtilisateurHelper.getUtilisateurCollection().document(uid).set(utilisateur);
    }

    // --- GET ---

    public static Task<DocumentSnapshot> getUtilisateur(String uid){
        return UtilisateurHelper.getUtilisateurCollection().document(uid).get();
    }

    // --- UPDATE ---

    public static Task<Void> updateUtilisateur(String uid, String pseudo, String genre, Date dateNaissance) {
        return UtilisateurHelper.getUtilisateurCollection().document(uid).update("pseudo", pseudo, "genre", genre, "dateNaissance", dateNaissance);
    }

    // --- DELETE ---

    public static Task<Void> supprimerUtilisateur(String uid) {
        return UtilisateurHelper.getUtilisateurCollection().document(uid).delete();
    }
}
