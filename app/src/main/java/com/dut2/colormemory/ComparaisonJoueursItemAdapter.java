package com.dut2.colormemory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dut2.colormemory.models.ComparaisonJoueursItem;

import java.util.List;

public class ComparaisonJoueursItemAdapter extends BaseAdapter {

    private Context context;
    private List<ComparaisonJoueursItem> comparaisonJoueursItemList;
    private LayoutInflater inflater;

    public ComparaisonJoueursItemAdapter(Context context, List<ComparaisonJoueursItem> comparaisonJoueursItemList) {
        this.context = context;
        this.comparaisonJoueursItemList = comparaisonJoueursItemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return comparaisonJoueursItemList.size();
    }

    @Override
    public ComparaisonJoueursItem getItem(int position) {
        return comparaisonJoueursItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_comparaisonjoueurs, null);

        ComparaisonJoueursItem item = getItem(position);
        String itemCategorie = item.getCategorie();

        TextView itemCategorieView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_categorie);
        itemCategorieView.setText(String.valueOf(itemCategorie)); //Affiche le Mode concerne par l'instance


        /* DEBUT
         * Affiche les donnees du mode concerne par l'instance
         */
        double nbPointUtilisateur = item.getNbPointUtilisateur();
        int nbPartieUtilisateur = item.getNbPartieUtilisateur();
        double moyenneUtilisateur = item.getMoyenneUtilisateur();
        double scoreMaxUtilisateur = item.getScoreMaxUtilisateur();

        TextView itemNbPointUtilisateurView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_nbPointUtilisateur);
        itemNbPointUtilisateurView.setText(String.valueOf(nbPointUtilisateur));
        TextView itemNbPartieUtilisateurView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_nbPartieUtilisateur);
        itemNbPartieUtilisateurView.setText(String.valueOf(nbPartieUtilisateur));
        TextView itemMoyenneUtilisateurView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_moyenneUtilisateur);
        itemMoyenneUtilisateurView.setText(String.valueOf(moyenneUtilisateur));
        TextView itemScoreMaxUtilisateurView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_scoreMaxUtilisateur);
        itemScoreMaxUtilisateurView.setText(String.valueOf(scoreMaxUtilisateur));

        double nbPointAdversaire = item.getNbPointAdversaire();
        int nbPartieAdversaire = item.getNbPartieAdversaire();
        double moyenneAdversaire = item.getMoyenneAdversaire();
        double scoreMaxAdversaire = item.getScoreMaxAdversaire();

        TextView itemNbPointAdversaireView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_nbPointAdversaire);
        itemNbPointAdversaireView.setText(String.valueOf(nbPointAdversaire));
        TextView itemNbPartieAdversaireView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_nbPartieAdversaire);
        itemNbPartieAdversaireView.setText(String.valueOf(nbPartieAdversaire));
        TextView itemMoyenneAdversaireView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_moyenneAdversaire);
        itemMoyenneAdversaireView.setText(String.valueOf(moyenneAdversaire));
        TextView itemScoreMaxAdversaireView = convertView.findViewById(R.id.id_comparaisonjoueur_textView_scoreMaxAdversaire);
        itemScoreMaxAdversaireView.setText(String.valueOf(scoreMaxAdversaire));

        /* FIN
         * Affiche les donnees du mode concerne par l'instance
         */



        /* DEBUT
         * Met de la couleur sur les score des joueur non identique
         */

        int vert = 0xff00ff00;
        int rouge = 0xffff0000;

        //Couleurs nombre de point(s)
        if (nbPointUtilisateur>nbPointAdversaire) {
            itemNbPointUtilisateurView.setTextColor(vert);
            itemNbPointAdversaireView.setTextColor(rouge);
        }
        else if (nbPointUtilisateur<nbPointAdversaire) {
            itemNbPointUtilisateurView.setTextColor(rouge);
            itemNbPointAdversaireView.setTextColor(vert);
        }

        //Couleurs nombre de partie(s)
        if (nbPartieUtilisateur>nbPartieAdversaire) {
            itemNbPartieUtilisateurView.setTextColor(rouge);
            itemNbPartieAdversaireView.setTextColor(vert);
        }
        else if (nbPartieUtilisateur<nbPartieAdversaire) {
            itemNbPartieUtilisateurView.setTextColor(vert);
            itemNbPartieAdversaireView.setTextColor(rouge);
        }

        //Couleurs moyenne
        if (moyenneUtilisateur>moyenneAdversaire) {
            itemMoyenneUtilisateurView.setTextColor(vert);
            itemMoyenneAdversaireView.setTextColor(rouge);
        }
        else if (moyenneUtilisateur<moyenneAdversaire) {
            itemMoyenneUtilisateurView.setTextColor(rouge);
            itemMoyenneAdversaireView.setTextColor(vert);
        }

        //Couleurs score max
        if (scoreMaxUtilisateur>scoreMaxAdversaire) {
            itemScoreMaxUtilisateurView.setTextColor(vert);
            itemScoreMaxAdversaireView.setTextColor(rouge);
        }
        else if (scoreMaxUtilisateur<scoreMaxAdversaire) {
            itemScoreMaxUtilisateurView.setTextColor(rouge);
            itemScoreMaxAdversaireView.setTextColor(vert);
        }

        /* FIN
         * Met de la couleur sur les score des joueur non identique
         */


        return convertView;
    }
}
