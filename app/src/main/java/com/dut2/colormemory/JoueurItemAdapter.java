package com.dut2.colormemory;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dut2.colormemory.models.JoueurItem;

import java.util.List;

import static androidx.core.content.ContextCompat.startActivity;

public class JoueurItemAdapter extends BaseAdapter {

    private Context context;
    private List<JoueurItem> joueurItemsList;
    private LayoutInflater inflater;

    public JoueurItemAdapter(Context context, List<JoueurItem> joueurItemsList) {
        this.context = context;
        this.joueurItemsList = joueurItemsList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return joueurItemsList.size();
    }

    @Override
    public JoueurItem getItem(int position) {
        return joueurItemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_classementjoueur, null);

        JoueurItem item = getItem(position);
        int itemRang = item.getRang();
        String itemPseudo = item.getPseudo();
        double itemTotalPoint = item.getTotalPoint();

        TextView itemRangView = convertView.findViewById(R.id.id_classementjoueur_textView_rang);
        itemRangView.setText(String.valueOf(itemRang));
        TextView itemPseudoView = convertView.findViewById(R.id.id_classementjoueur_textView_pseudo);
        itemPseudoView.setText(String.valueOf(itemPseudo));
        TextView itemTotalPointView = convertView.findViewById(R.id.id_classementjoueur_textView_totalPoint);
        itemTotalPointView.setText(String.valueOf(itemTotalPoint));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ComparaisonJoueurs.class);
                intent.putExtra("pseudoAdversaire", itemPseudo);
                intent.putExtra("idAdversaire", item.getId());
                startActivity(context, intent, null);
            }
        });

        return convertView;
    }
}
