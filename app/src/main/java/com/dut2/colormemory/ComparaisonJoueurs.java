package com.dut2.colormemory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.dut2.colormemory.api.UtilisateurDonneesJeuxHelper;
import com.dut2.colormemory.api.UtilisateurHelper;
import com.dut2.colormemory.base.Base;
import com.dut2.colormemory.models.ComparaisonJoueursItem;
import com.dut2.colormemory.models.Utilisateur;
import com.dut2.colormemory.models.UtilisateurDonneesJeux;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

/*
 * Permet de comparer ses scores avec un autre joueur (selectioner dans le classement du menu)
 */

public class ComparaisonJoueurs extends Base {

    private String pseudoAdversaire, idAdversaire;
    private TextView pseudoUtilisateurTextView, pseudoAdversaireTextView;
    private List<ComparaisonJoueursItem> comparaisonJoueursItemList;
    private ListView comparaisonJoueursListView;

    private Utilisateur utilisateur;
    private UtilisateurDonneesJeux utilisateurDonneesJeux;
    private UtilisateurDonneesJeux adversaireDonneesJeux;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparaisonjoueurs);

        pseudoAdversaire = getIntent().getStringExtra("pseudoAdversaire");
        idAdversaire = getIntent().getStringExtra("idAdversaire");

        comparaisonJoueursListView = findViewById(R.id.id_comparaisonjoueurs_listView_comparaison);
        pseudoUtilisateurTextView = findViewById(R.id.id_comparaisonjoueurs_textView_pseudoUtilisateur);
        pseudoAdversaireTextView = findViewById(R.id.id_comparaisonjoueurs_textView_pseudoAdversaire);
        pseudoAdversaireTextView.setText(pseudoAdversaire);

        updateDonneeUtilisateur();
        updateDonneeNiveauUtilisateur();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_comparaisonjoueurs;
    }



    /*
     * Les 3 mothode qui suivent permettent d'obtenir les données necessaire a la comparaison
     */
    private void updateDonneeUtilisateur() {
        UtilisateurHelper.getUtilisateur(this.getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                utilisateur = documentSnapshot.toObject(Utilisateur.class);
                pseudoUtilisateurTextView.setText(utilisateur.getPseudo());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
    }

    private void updateDonneeNiveauUtilisateur() {
        UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeux(getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                utilisateurDonneesJeux = documentSnapshot.toObject(UtilisateurDonneesJeux.class);
                updateDonneeNiveauAdversaire();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
    }

    private void updateDonneeNiveauAdversaire() {
        UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeux(idAdversaire).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                adversaireDonneesJeux = documentSnapshot.toObject(UtilisateurDonneesJeux.class);
                completionListView();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
    }



    /*
     * Creer une comparaison des joueur pour chaque Mode (et le total)
     */

    private void completionListView() {

        comparaisonJoueursItemList = new ArrayList<>();
        comparaisonJoueursItemList.add(new ComparaisonJoueursItem(getString(R.string.facile),
                utilisateurDonneesJeux.getFacilePoints(),
                (int) utilisateurDonneesJeux.getFacileNbPartie(),
                utilisateurDonneesJeux.getFacileMoyenne(),
                utilisateurDonneesJeux.getFacileScoreMax(),
                adversaireDonneesJeux.getFacilePoints(),
                (int) adversaireDonneesJeux.getFacileNbPartie(),
                adversaireDonneesJeux.getFacileMoyenne(),
                adversaireDonneesJeux.getFacileScoreMax()));

        comparaisonJoueursItemList.add(new ComparaisonJoueursItem(getString(R.string.difficile),
                utilisateurDonneesJeux.getDifficilePoints(),
                (int) utilisateurDonneesJeux.getDifficileNbPartie(),
                utilisateurDonneesJeux.getDifficileMoyenne(),
                utilisateurDonneesJeux.getDifficileScoreMax(),
                adversaireDonneesJeux.getDifficilePoints(),
                (int) adversaireDonneesJeux.getDifficileNbPartie(),
                adversaireDonneesJeux.getDifficileMoyenne(),
                adversaireDonneesJeux.getDifficileScoreMax()));

        comparaisonJoueursItemList.add(new ComparaisonJoueursItem(getString(R.string.expert),
                utilisateurDonneesJeux.getExpertPoints(),
                (int) utilisateurDonneesJeux.getExpertNbPartie(),
                utilisateurDonneesJeux.getExpertMoyenne(),
                utilisateurDonneesJeux.getExpertScoreMax(),
                adversaireDonneesJeux.getExpertPoints(),
                (int) adversaireDonneesJeux.getExpertNbPartie(),
                adversaireDonneesJeux.getExpertMoyenne(),
                adversaireDonneesJeux.getExpertScoreMax()));

        comparaisonJoueursItemList.add(new ComparaisonJoueursItem(getString(R.string.chrono),
                utilisateurDonneesJeux.getChronoPoints(),
                (int) utilisateurDonneesJeux.getChronoNbPartie(),
                utilisateurDonneesJeux.getChronoMoyenne(),
                utilisateurDonneesJeux.getChronoScoreMax(),
                adversaireDonneesJeux.getChronoPoints(),
                (int) adversaireDonneesJeux.getChronoNbPartie(),
                adversaireDonneesJeux.getChronoMoyenne(),
                adversaireDonneesJeux.getChronoScoreMax()));

        comparaisonJoueursItemList.add(new ComparaisonJoueursItem(getString(R.string.total),
                utilisateurDonneesJeux.getTotalPoint(),
                (int) utilisateurDonneesJeux.getTotalNbPartie(),
                utilisateurDonneesJeux.getTotalMoyenne(),
                utilisateurDonneesJeux.getTotalScoreMax(),
                adversaireDonneesJeux.getTotalPoint(),
                (int) adversaireDonneesJeux.getTotalNbPartie(),
                adversaireDonneesJeux.getTotalMoyenne(),
                adversaireDonneesJeux.getTotalScoreMax()));

        comparaisonJoueursListView.setAdapter(new ComparaisonJoueursItemAdapter(this, comparaisonJoueursItemList));
    }









    /* DEBUT
     * Gestion de la barre de navigation
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_navigation, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.id_menu_navigation_profil:
                intent = new Intent(this, Profil.class);
                startActivity(intent);
                return true;
            case R.id.id_menu_navigation_deconnexion:
                getmAuth().signOut();
                intent = new Intent(this, Connexion.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* FIN
     * Gestion de la barre de navigation
     */


    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ComparaisonJoueurs.this, com.dut2.colormemory.Menu.class);
        startActivity(intent);
    }
}