package com.dut2.colormemory;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dut2.colormemory.base.Base;

/*
 * Permet d'informer d'une enventuelle victoire ou d'une echec.
 * Permet aussi de rejouer (au meme mode) ou de revenir au menu
 */

public class PostNiveaux extends Base {

    private double [] donneesMode;
    private double nbScore;
    private String typeFin;
    private TextView scoreTextView, typeFinTextView;
    private Button rejouerButton, menuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postniveaux);


        donneesMode = getIntent().getDoubleArrayExtra("donneesModeUtilise");

        nbScore = getIntent().getDoubleExtra("score", 0);
        scoreTextView = findViewById(R.id.id_perdu_textView_score);
        scoreTextView.setText(String.valueOf(nbScore));

        typeFin = getIntent().getStringExtra("typeFin");
        typeFinTextView = findViewById(R.id.id_perdu_textView_typeFin);
        typeFinTextView.setText(typeFin);


        rejouerButton = findViewById(R.id.id_perdu_button_rejouer);
        rejouerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PostNiveaux.this, Niveaux.class);
                intent.putExtra("donnéesMode", donneesMode);
                startActivity(intent);
            }
        });

        menuButton = findViewById(R.id.id_perdu_button_menu);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PostNiveaux.this, Menu.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_postniveaux;
    }










    /* DEBUT
     * Gestion de la barre de navigation
     */

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.id_menu_navigation_profil:
                intent = new Intent(this, Profil.class);
                startActivity(intent);
                return true;
            case R.id.id_menu_navigation_deconnexion:
                getmAuth().signOut();
                intent = new Intent(this, Connexion.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* FIN
     * Gestion de la barre de navigation
     */


    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PostNiveaux.this, com.dut2.colormemory.Menu.class);
        startActivity(intent);
    }
}