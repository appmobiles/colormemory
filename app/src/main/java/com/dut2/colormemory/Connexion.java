package com.dut2.colormemory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.dut2.colormemory.base.Base;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

/*
 * Permet a l'utilisateur de se connecte
 */

public class Connexion extends Base {

    private String email, mdp;

    private TextView sIncrireTextView;
    private Button seConnecterButton;
    private EditText emailEditText, mdpEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        this.emailEditText = findViewById(R.id.id_connexion_editText_email);
        this.mdpEditText = findViewById(R.id.id_connexion_editText_mdp);

        seConnecterButton = findViewById(R.id.id_connexion_button_seConnecter);
        seConnecterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailEditText.getText().toString();
                mdp = mdpEditText.getText().toString();

                if (email.isEmpty()) {
                    Toast.makeText(Connexion.this, getString(R.string.veuillez_renseigner_un_email), Toast.LENGTH_SHORT).show();
                }
                else if (mdp.isEmpty() || mdpEditText.length()<6) {
                    Toast.makeText(Connexion.this, getString(R.string.veuillez_renseigner_un_mot_de_passe), Toast.LENGTH_SHORT).show();
                }
                else {
                    connexion();
                }
            }
        });

        sIncrireTextView = findViewById(R.id.id_connexion_textView_sInscrire);
        sIncrireTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Connexion.this, Inscription.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_connexion;
    }





    private void connexion() {
        getmAuth().signInWithEmailAndPassword(email, mdp)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            connexionTermine();
                        } else {
                            onFailureListener();
                        }
                    }
                });
    }

    private void connexionTermine() {
        Toast.makeText(Connexion.this, getString(R.string.vous_etes_connecte), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Connexion.this, Menu.class);
        startActivity(intent);
    }









    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {

    }
}