package com.dut2.colormemory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.dut2.colormemory.api.UtilisateurClassementHelper;
import com.dut2.colormemory.api.UtilisateurDonneesJeuxHelper;
import com.dut2.colormemory.api.UtilisateurHelper;
import com.dut2.colormemory.base.Base;
import com.dut2.colormemory.models.Utilisateur;
import com.dut2.colormemory.models.UtilisateurDonneesJeux;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Permet d'afficher toutes les informations de l'utilisateur connecte (Confidentielle et donnees jeux)
 * Propose egalement des optin de gestion pour le compte (modification information, changement MdP et suppression du compte)
 */

public class Profil extends Base {

    private int niveauChoisi;
    public String genre;

    private TextView pseudoTextView, dateNaissanceTextView, ageTextView, genreTextView;
    private TextView supprimerCompte, modifierCompte, changerMotDePasse;
    private TextView categorieTextView, nbPointsTextView, nbPartiesTextView, moyenneTextView, scoreMaxTextView;

    private Switch totalSwitch;
    private TabLayout niveauxTabLayout;

    static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private Utilisateur utilisateur;
    private UtilisateurDonneesJeux utilisateurDonneesJeux;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);


        totalSwitch = findViewById(R.id.id_profil_switch_total);
        niveauxTabLayout = findViewById(R.id.id_profil_tabLayout_niveaux);
        pseudoTextView = findViewById(R.id.id_profil_textView_pseudo);
        dateNaissanceTextView = findViewById(R.id.id_profil_textView_dateNaissance);
        ageTextView = findViewById(R.id.id_profil_textView_age);
        genreTextView = findViewById(R.id.id_profil_textView_genre);
        updateDonneeUtilisateur();

        categorieTextView = findViewById(R.id.id_profil_textView_categorie);
        nbPointsTextView = findViewById(R.id.id_profil_textView_nbPoints);
        nbPartiesTextView = findViewById(R.id.id_profil_textView_nbParties);
        moyenneTextView = findViewById(R.id.id_profil_textView_moyenne);
        scoreMaxTextView = findViewById(R.id.id_profil_textView_scoreMax);
        updateDonneeNiveau();


        /* DEBUT
         * Popups
         */
        modifierCompte = findViewById(R.id.id_profil_textView_modifierCompte);
        modifierCompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupModificationCompte();
            }
        });


        changerMotDePasse = findViewById(R.id.id_profil_textView_changerMotDePasse);
        changerMotDePasse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupChangementMotDePasse();
            }
        });


        supprimerCompte = findViewById(R.id.id_profil_textView_supprimerCompte);
        supprimerCompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupSuppressionCompte();
            }
        });

        /* Fin
         * Popups
         */



        totalSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    niveauxTabLayout.setBackgroundColor(0xff008080);
                    niveauxTabLayout.setSelectedTabIndicatorColor(0xff800080);
                    niveauxTabLayout.setTabTextColors(0xff20062e,0xff800080);
                    categorieTextView.setText(getString(R.string.total));
                    nbPointsTextView.setText(String.valueOf(utilisateurDonneesJeux.getTotalPoint()));
                    nbPartiesTextView.setText(String.valueOf(utilisateurDonneesJeux.getTotalNbPartie()));
                    moyenneTextView.setText(String.valueOf(utilisateurDonneesJeux.getTotalMoyenne()));
                    scoreMaxTextView.setText(String.valueOf(utilisateurDonneesJeux.getTotalScoreMax()));
                }
                else {
                    niveauxTabLayout.setBackgroundColor(0xff00ffff);
                    niveauxTabLayout.setSelectedTabIndicatorColor(0xffff00ff);
                    niveauxTabLayout.setTabTextColors(0xff20062e,0xffff00ff);
                    afficheDonneeNiveau();
                }
            }
        });

        niveauxTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                afficheDonneeNiveau();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_profil;
    }



    /* DEBUT
     * Recupere et affiche les donnees de l'utilisateur
     */

    private void updateDonneeUtilisateur() {
        UtilisateurHelper.getUtilisateur(this.getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                utilisateur = documentSnapshot.toObject(Utilisateur.class);
                afficheDonneeUtilisateur();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
    }

    private void afficheDonneeUtilisateur() {
        pseudoTextView.setText(utilisateur.getPseudo());
        dateNaissanceTextView.setText(dateFormat.format(utilisateur.getDateNaissance()));
        ageTextView.setText(String.valueOf(utilisateur.getAge()));
        String genre = utilisateur.getGenre();
        if (genre.equals("fille")) {
            genreTextView.setText(getString(R.string.fille));
        }
        else if (genre.equals("garcon")) {
            genreTextView.setText(getString(R.string.garcon));
        }
        else {
            genreTextView.setText(getString(R.string.autre));
        }
    }


    private void updateDonneeNiveau() {
        UtilisateurDonneesJeuxHelper.getUtilisateurDonneesJeux(getUtilisateurConnecte().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                utilisateurDonneesJeux = documentSnapshot.toObject(UtilisateurDonneesJeux.class);
                afficheDonneeNiveau();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onFailureListener();
            }
        });
    }

    private void afficheDonneeNiveau() {
        niveauChoisi = niveauxTabLayout.getSelectedTabPosition();
        if (!totalSwitch.isChecked())
        {
            switch (niveauChoisi) {
                case 1: //Difficile
                    categorieTextView.setText(getString(R.string.difficile));
                    nbPointsTextView.setText(String.valueOf(utilisateurDonneesJeux.getDifficilePoints()));
                    nbPartiesTextView.setText(String.valueOf(utilisateurDonneesJeux.getDifficileNbPartie()));
                    moyenneTextView.setText(String.valueOf(utilisateurDonneesJeux.getDifficileMoyenne()));
                    scoreMaxTextView.setText(String.valueOf(utilisateurDonneesJeux.getDifficileScoreMax()));
                    break;
                case 2: //Expert
                    categorieTextView.setText(getString(R.string.expert));
                    nbPointsTextView.setText(String.valueOf(utilisateurDonneesJeux.getExpertPoints()));
                    nbPartiesTextView.setText(String.valueOf(utilisateurDonneesJeux.getExpertNbPartie()));
                    moyenneTextView.setText(String.valueOf(utilisateurDonneesJeux.getExpertMoyenne()));
                    scoreMaxTextView.setText(String.valueOf(utilisateurDonneesJeux.getExpertScoreMax()));
                    break;
                case 3: //Chrono
                    categorieTextView.setText(getString(R.string.chrono));
                    nbPointsTextView.setText(String.valueOf(utilisateurDonneesJeux.getChronoPoints()));
                    nbPartiesTextView.setText(String.valueOf(utilisateurDonneesJeux.getChronoNbPartie()));
                    moyenneTextView.setText(String.valueOf(utilisateurDonneesJeux.getChronoMoyenne()));
                    scoreMaxTextView.setText(String.valueOf(utilisateurDonneesJeux.getChronoScoreMax()));
                    break;
                default: //Facile (et autre si pbr)
                    categorieTextView.setText(getString(R.string.facile));
                    nbPointsTextView.setText(String.valueOf(utilisateurDonneesJeux.getFacilePoints()));
                    nbPartiesTextView.setText(String.valueOf(utilisateurDonneesJeux.getFacileNbPartie()));
                    moyenneTextView.setText(String.valueOf(utilisateurDonneesJeux.getFacileMoyenne()));
                    scoreMaxTextView.setText(String.valueOf(utilisateurDonneesJeux.getFacileScoreMax()));
                    break;
            }
        }
    }

    /* FIN
     * Recupere et affiche les donnees de l'utilisateur
     */



    //Permet de mettre la date avec le bon format
    private Date getDateNaissance(String dateNaissance){
        try {
            Date date = dateFormat.parse(dateNaissance);
            return date;
        } catch (ParseException e){
            return new Date() ;
        }
    }



    private void popupModificationCompte() {
        LinearLayout layout = new LinearLayout(Profil.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        EditText pseudo = new EditText(Profil.this);
        pseudo.setText(utilisateur.getPseudo());
        layout.addView(pseudo);

        EditText dateNaissance = new EditText(Profil.this);
        dateNaissance.setText(dateFormat.format(utilisateur.getDateNaissance()));
        dateNaissance.setInputType(InputType.TYPE_CLASS_DATETIME);
        layout.addView(dateNaissance);

        LinearLayout layoutSecondaire = new LinearLayout(Profil.this);
        layoutSecondaire.setOrientation(LinearLayout.HORIZONTAL);

        CheckBox fille = new CheckBox(Profil.this);
        fille.setText("fille");
        layoutSecondaire.addView(fille);
        CheckBox garcon = new CheckBox(Profil.this);
        garcon.setText("garcon");
        layoutSecondaire.addView(garcon);
        CheckBox autre = new CheckBox(Profil.this);
        autre.setText("autre");
        layoutSecondaire.addView(autre);


        fille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fille.setChecked(true);
                garcon.setChecked(false);
                autre.setChecked(false);
                genre = "fille";
            }
        });
        garcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fille.setChecked(false);
                garcon.setChecked(true);
                autre.setChecked(false);
                genre = "garcon";
            }
        });
        autre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fille.setChecked(false);
                garcon.setChecked(false);
                autre.setChecked(true);
                genre = "autre";
            }
        });


        if (utilisateur.getGenre().compareTo("fille") == 0) {
            fille.setChecked(true);
            genre = "fille";
        }
        else if (utilisateur.getGenre().compareTo("garcon") == 0) {
            garcon.setChecked(true);
            genre = "garcon";
        }
        else {
            autre.setChecked(true);
            genre = "autre";
        }

        layout.addView(layoutSecondaire);


        new AlertDialog.Builder(Profil.this)
                .setTitle(getString(R.string.popup_modification_compte_titre))
                .setMessage(getString(R.string.popup_modification_compte_message))
                .setView(layout)
                .setPositiveButton(getString(R.string.appliquer), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Verification des differents champs saisie
                        if (pseudo.getText().toString().isEmpty()) {
                            Toast.makeText(Profil.this, getString(R.string.veuillez_renseigner_un_pseudo), Toast.LENGTH_SHORT).show();
                        }
                        else if (dateNaissance.getText().toString().isEmpty()) {
                            Toast.makeText(Profil.this, getString(R.string.veuillez_renseigner_votre_date_de_naissance), Toast.LENGTH_SHORT).show();
                        }
                        else if (genre == null) {
                            Toast.makeText(Profil.this, getString(R.string.veuillez_renseigner_votre_genre), Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Date date = getDateNaissance(dateNaissance.getText().toString());
                            UtilisateurHelper.updateUtilisateur(getUtilisateurConnecte().getUid(), pseudo.getText().toString(), genre, date).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    onFailureListener();
                                }
                            });
                            if (utilisateur.getPseudo().equals(pseudo.getText().toString()))
                            {
                                UtilisateurClassementHelper.updatePseudo(getUtilisateurConnecte().getUid(), pseudo.getText().toString()).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        onFailureListener();
                                    }
                                });
                            }
                            updateDonneeUtilisateur();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }


    private void popupChangementMotDePasse() {
        LinearLayout layout = new LinearLayout(Profil.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        EditText email = new EditText(Profil.this);
        email.setHint(getString(R.string.email));
        layout.addView(email);

        EditText mdp = new EditText(Profil.this);
        mdp.setHint(getString(R.string.mot_de_passe));
        mdp.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        layout.addView(mdp);

        EditText nouveauMdp = new EditText(Profil.this);
        nouveauMdp.setHint(getString(R.string.nouveau_mot_de_passe));
        nouveauMdp.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        layout.addView(nouveauMdp);

        EditText confirmerNouveuMdp = new EditText(Profil.this);
        confirmerNouveuMdp.setHint(getString(R.string.confirmer_le_nouveau_MDP));
        confirmerNouveuMdp.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        layout.addView(confirmerNouveuMdp);

        new AlertDialog.Builder(Profil.this)
                .setTitle(getString(R.string.popup_changement_mdp_title))
                .setMessage(getString(R.string.popup_changement_mdp_message))
                .setView(layout)
                .setPositiveButton(getString(R.string.confirmer), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (email.getText().toString().isEmpty() || mdp.getText().toString().isEmpty() || mdp.length()<6) {}
                        else if (nouveauMdp.getText().toString().isEmpty() || nouveauMdp.length()<6 ||
                                !confirmerNouveuMdp.getText().toString().equals(nouveauMdp.getText().toString()) ) {}
                        else {
                            changerMotDePasse(email.getText().toString().replaceAll(" ",""), mdp.getText().toString(), nouveauMdp.getText().toString());
                        }
                    }
                })
                .setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }


    private void popupSuppressionCompte() {
        new AlertDialog.Builder(Profil.this)
                .setTitle(getString(R.string.popup_supprimer_compte_title))
                .setMessage(getString(R.string.popup_supprimer_compte_message_1))
                .setPositiveButton(getString(R.string.oui), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        LinearLayout layout = new LinearLayout(Profil.this);
                        layout.setOrientation(LinearLayout.VERTICAL);

                        EditText email = new EditText(Profil.this);
                        email.setHint(getString(R.string.email));
                        layout.addView(email);

                        EditText mdp = new EditText(Profil.this);
                        mdp.setHint(getString(R.string.mot_de_passe));
                        mdp.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        layout.addView(mdp);

                        //Debut second popup
                        new AlertDialog.Builder(Profil.this)
                                .setTitle(getString(R.string.popup_supprimer_compte_title))
                                .setMessage(R.string.popup_supprimer_compte_message_2)
                                .setView(layout)
                                .setPositiveButton(getString(R.string.supprimer), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!email.getText().toString().isEmpty() || !mdp.getText().toString().isEmpty() || mdp.length()>5) {
                                            supprimerCompte(email.getText().toString().replaceAll(" ",""), mdp.getText().toString());
                                        }
                                    }
                                })
                                .setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                        //Fin second popup

                    }
                })
                .setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }



    // Changer le mot de passe
    private void changerMotDePasse(String email, String mdp, String nouveauMdp) {

        AuthCredential credential = EmailAuthProvider.getCredential(email, mdp);
        getUtilisateurConnecte().reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            getUtilisateurConnecte().updatePassword(nouveauMdp)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(Profil.this, getString(R.string.votre_mot_de_passe_a_bien_ete_modifie), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        } else {
                            onFailureListener();
                        }
                    }
                });
    }


    // Permet de supprimer le compte d'un joueur
    private void supprimerCompte(String email, String mdp) {

        AuthCredential credential = EmailAuthProvider.getCredential(email, mdp);
        getUtilisateurConnecte().reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            String id = getUtilisateurConnecte().getUid();
                            getUtilisateurConnecte().delete()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                UtilisateurClassementHelper.supprimerUtilisateurClassement(id).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        onFailureListener();
                                                    }
                                                });
                                                UtilisateurHelper.supprimerUtilisateur(id).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        onFailureListener();
                                                    }
                                                });
                                                UtilisateurDonneesJeuxHelper.supprimerUtilisateurDonneesJeux(id).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        onFailureListener();
                                                    }
                                                });

                                                Toast.makeText(Profil.this, getString(R.string.votre_compte_a_bien_ete_supprime), Toast.LENGTH_SHORT).show();

                                                getmAuth().signOut();
                                                Intent intent = new Intent(Profil.this, Connexion.class);
                                                startActivity(intent);
                                            }
                                        }
                                    });


                        } else {
                            Toast.makeText(Profil.this, getString(R.string.une_erreur_est_survenue), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }










    /* DEBUT
     * Gestion de la barre de navigation
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.id_menu_navigation_profil:
                intent = new Intent(this, Profil.class);
                startActivity(intent);
                return true;
            case R.id.id_menu_navigation_deconnexion:
                getmAuth().signOut();
                intent = new Intent(this, Connexion.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* FIN
     * Gestion de la barre de navigation
     */


    //Interception de la touche retour de l'appareil
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Profil.this, com.dut2.colormemory.Menu.class);
        startActivity(intent);
    }
}